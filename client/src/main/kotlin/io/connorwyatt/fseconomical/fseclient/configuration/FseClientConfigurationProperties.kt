package io.connorwyatt.fseconomical.fseclient.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "fse-client")
class FseClientConfigurationProperties {
  var baseUrl: String? = null
}
