package io.connorwyatt.fseconomical.fseclient.models

import com.fasterxml.jackson.annotation.JsonProperty

class Assignment {
  @JsonProperty("Id")
  lateinit var id: Number

  @JsonProperty("Location")
  lateinit var location: String

  @JsonProperty("ToIcao")
  lateinit var toIcao: String

  @JsonProperty("FromIcao")
  lateinit var fromIcao: String

  @JsonProperty("Amount")
  lateinit var amount: Number

  @JsonProperty("UnitType")
  lateinit var unitType: String

  @JsonProperty("Commodity")
  lateinit var commodity: String

  @JsonProperty("Pay")
  lateinit var pay: Number

  @JsonProperty("Expires")
  lateinit var expires: String

  @JsonProperty("ExpireDateTime")
  lateinit var expireDateTime: String

  @JsonProperty("Type")
  lateinit var type: String

  @JsonProperty("Express")
  lateinit var express: String

  @JsonProperty("PtAssignment")
  var ptAssignment: Boolean = false

  @JsonProperty("AircraftId")
  lateinit var aircraftId: Number
}
