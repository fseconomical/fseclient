package io.connorwyatt.fseconomical.fseclient.configuration

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import io.connorwyatt.fseconomical.fseclient.FseClient
import io.connorwyatt.fseconomical.fseclient.HttpFseClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class FseClientConfiguration {
  @Bean
  fun fseClient(
    restTemplate: RestTemplate,
    xmlMapper: XmlMapper,
    configuration: FseClientConfigurationProperties
  ): FseClient {
    return HttpFseClient(restTemplate, xmlMapper, configuration)
  }
}
