package io.connorwyatt.fseconomical.fseclient.models

enum class AssignmentDirection {
  From,
  To
}
