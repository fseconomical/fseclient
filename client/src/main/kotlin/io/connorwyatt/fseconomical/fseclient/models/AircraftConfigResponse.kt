package io.connorwyatt.fseconomical.fseclient.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

@JsonIgnoreProperties(ignoreUnknown = true)
internal class AircraftConfigResponse {
  @JacksonXmlElementWrapper(useWrapping = false)
  @JsonProperty("AircraftConfig")
  lateinit var configurations: List<AircraftConfig>
}
