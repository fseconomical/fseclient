package io.connorwyatt.fseconomical.fseclient.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

@JsonIgnoreProperties(ignoreUnknown = true)
internal class AircraftAliasResponse {
  @JacksonXmlElementWrapper(useWrapping = false)
  @JsonProperty("AircraftAliases")
  lateinit var aircraftAliases: List<AircraftAlias>
}
