package io.connorwyatt.fseconomical.fseclient.models

import com.fasterxml.jackson.annotation.JsonProperty

class AircraftConfig {
  @JsonProperty("MakeModel")
  lateinit var makeModel: String

  @JsonProperty("Crew")
  lateinit var crew: Number

  @JsonProperty("Seats")
  lateinit var seats: Number

  @JsonProperty("CruiseSpeed")
  lateinit var cruiseSpeed: Number

  @JsonProperty("GPH")
  lateinit var gph: Number

  @JsonProperty("FuelType")
  lateinit var fuelType: Number

  @JsonProperty("MTOW")
  lateinit var mtow: Number

  @JsonProperty("EmptyWeight")
  lateinit var emptyWeight: Number

  @JsonProperty("Price")
  lateinit var price: Number

  @JsonProperty("Ext1")
  lateinit var ext1: Number

  @JsonProperty("LTip")
  lateinit var lTip: Number

  @JsonProperty("LAux")
  lateinit var lAux: Number

  @JsonProperty("LMain")
  lateinit var lMain: Number

  @JsonProperty("Center1")
  lateinit var center1: Number

  @JsonProperty("Center2")
  lateinit var center2: Number

  @JsonProperty("Center3")
  lateinit var center3: Number

  @JsonProperty("RMain")
  lateinit var rMain: Number

  @JsonProperty("RAux")
  lateinit var rAux: Number

  @JsonProperty("RTip")
  lateinit var rTip: Number

  @JsonProperty("Ext2")
  lateinit var ext2: Number

  @JsonProperty("Engines")
  lateinit var engines: Number

  @JsonProperty("EnginePrice")
  lateinit var enginePrice: Number

  @JsonProperty("ModelId")
  lateinit var modelId: Number
}
