package io.connorwyatt.fseconomical.fseclient.models

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

class AircraftAlias {
  @JsonProperty("MakeModel")
  lateinit var makeModel: String

  @JacksonXmlElementWrapper(useWrapping = false)
  @JsonProperty("Alias")
  lateinit var aliases: List<String>
}
