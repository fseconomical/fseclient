package io.connorwyatt.fseconomical.fseclient.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

@JsonIgnoreProperties(ignoreUnknown = true)
internal class IcaoJobsResponse {
  @JacksonXmlElementWrapper(useWrapping = false)
  @JsonProperty("Assignment")
  lateinit var assignments: List<Assignment>
}
