package io.connorwyatt.fseconomical.fseclient.configuration

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class XmlMapperConfiguration {
  @Bean
  fun xmlMapper(): XmlMapper {
    return XmlMapper()
  }
}
