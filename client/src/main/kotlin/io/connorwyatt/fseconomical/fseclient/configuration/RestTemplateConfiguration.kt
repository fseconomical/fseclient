package io.connorwyatt.fseconomical.fseclient.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

@Configuration
class RestTemplateConfiguration {
  @Bean
  fun restTemplate(): RestTemplate {
    val factory = SimpleClientHttpRequestFactory()
    factory.setOutputStreaming(false)
    return RestTemplate(factory)
  }
}
