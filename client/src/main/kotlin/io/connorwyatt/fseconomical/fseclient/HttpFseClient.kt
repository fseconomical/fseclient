package io.connorwyatt.fseconomical.fseclient

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.connorwyatt.fseconomical.fseclient.configuration.FseClientConfigurationProperties
import io.connorwyatt.fseconomical.fseclient.models.AircraftAlias
import io.connorwyatt.fseconomical.fseclient.models.AircraftAliasResponse
import io.connorwyatt.fseconomical.fseclient.models.AircraftConfig
import io.connorwyatt.fseconomical.fseclient.models.AircraftConfigResponse
import io.connorwyatt.fseconomical.fseclient.models.Assignment
import io.connorwyatt.fseconomical.fseclient.models.AssignmentDirection
import io.connorwyatt.fseconomical.fseclient.models.AssignmentDirection.From
import io.connorwyatt.fseconomical.fseclient.models.IcaoJobsResponse
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import org.springframework.web.util.UriComponentsBuilder

class HttpFseClient constructor(
  private val restTemplate: RestTemplate,
  private val mapper: XmlMapper,
  private val configuration: FseClientConfigurationProperties
) : FseClient {
  override fun getAliases(accessKey: String): List<AircraftAlias>? {
    return get<AircraftAliasResponse>(accessKey) {
      it.queryParam("query", "aircraft")
        .queryParam("search", "aliases")
    }?.aircraftAliases
  }

  override fun getAircraftConfigurations(accessKey: String): List<AircraftConfig>? {
    return get<AircraftConfigResponse>(accessKey) {
      it.queryParam("query", "aircraft")
        .queryParam("search", "configs")
    }?.configurations
  }

  override fun getAvailableAssignments(
    accessKey: String,
    icaoCodes: List<String>,
    direction: AssignmentDirection
  ): List<Assignment>? {
    return get<IcaoJobsResponse>(accessKey) {
      it.queryParam("query", "icao")
        .queryParam("search", if (direction == From) "jobsfrom" else "jobsto")
        .queryParam("icao", icaoCodes.joinToString("-"))
    }?.assignments
  }

  private inline fun <reified T> get(
    accessKey: String,
    addParams: (UriComponentsBuilder) -> UriComponentsBuilder
  ): T? {
    val uri = baseUri()
      .queryParam("userkey", accessKey)
      .run { addParams(this) }

    val response = restTemplate.getForEntity<String>(uri.toUriString())

    return response.body?.let { mapper.readValue(it) }
  }

  private fun baseUri(): UriComponentsBuilder {
    return UriComponentsBuilder.fromHttpUrl("${configuration.baseUrl}/data")
      .queryParam("format", "xml")
  }
}
