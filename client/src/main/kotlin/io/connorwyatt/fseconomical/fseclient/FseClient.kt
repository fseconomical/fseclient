package io.connorwyatt.fseconomical.fseclient

import io.connorwyatt.fseconomical.fseclient.models.AircraftAlias
import io.connorwyatt.fseconomical.fseclient.models.AircraftConfig
import io.connorwyatt.fseconomical.fseclient.models.Assignment
import io.connorwyatt.fseconomical.fseclient.models.AssignmentDirection

interface FseClient {
  fun getAliases(accessKey: String): List<AircraftAlias>?

  fun getAircraftConfigurations(accessKey: String): List<AircraftConfig>?

  fun getAvailableAssignments(
    accessKey: String,
    icaoCodes: List<String>,
    direction: AssignmentDirection
  ): List<Assignment>?
}
